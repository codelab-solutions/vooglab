
//
//
//  VoogLab™ - Codelab helper routines for Voog
//  2017 (c) Codelab Solutions OÜ <codelab@codelab.ee>
//
//


voogLab = {


	/**
	 *   Redirect
	 *   url  -  URL to redirect to
	 */

	redirect: function( url ) {
		window.location.href=url;
	},


	/**
	 *   Reload page
	 */

	reload: function() {
		window.location.reload(true);
	},


	/**
	 *   Open modal dialog
	 *   content  -  Modal content
	 *   modal_tag  -  Modal tag (optional, will be auto-created if not specified)
	 */

	openModal: function( content, modal_tag ) {
		if (modal_tag == null) {
			modal_tag='modal_' + Math.floor(Math.random()*100000+1);
		}
		var modal_html='<div class="modal_page edy-modal-mask edy-modal-mask-light edy-page-modal" id="modal_'+modal_tag+'">';
		modal_html=modal_html+'<div class="edy-modal edy-modal-large-width edy-modal-light">';
		modal_html=modal_html+'<div class="edy-modal-x" onclick="voogLab.closeModal(\''+modal_tag+'\')"><!--?xml version="1.0" encoding="UTF-8" standalone="no"?--><svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="close-big" fill="none" stroke-width="2" stroke="#000000"><path class="shape" d="M1,1 L19,19 M1,19 L19,1"></path></g></svg></div>';
		modal_html=modal_html+'<div class="edy-modal-pad">';
		modal_html=modal_html+'<p>'+content+'</p>';
		modal_html=modal_html+'</div>';
		modal_html=modal_html+'</div>';
		modal_html=modal_html+'</div>';
		$('body').append(modal_html);
		return modal_tag;
	},


	/**
	 *   Close modal dialog
	 *   modal_tag  -  Modal tag (optional, will be auto-created if not specified)
	 *   confirm_msg  -  Confirm close
	 */

	closeModal: function( modal_tag, confirm_msg ) {
		if (confirm_msg == null || confirm_msg === false) {
			$("#modal_"+modal_tag).remove();
		}
		else {
			if (confirm_msg === true) {
				confirm_msg='Close?';
			}
			if (confirm(confirm_msg)) {
				voogLab.closeModal(modal_tag,false);
			}
		}
	},


	/**
	 *    Element editor
	 */

	elementEditor: {


		/**
		 *   Open add element dialog
		 *   page_id  -  Voog page ID
		 *   element_definition_id  -  Voog element definition ID
		 *   show_elements  -  If not null, an array of elements to show
		 *   default_values  -  If not null, object of default values
		 *   hidden_data  -  If not null, object of hidden data to pass along with submit
		 *   options  -  Options
		 *     modal_title  -  Modal title
		 *     title_label  -  Element title label
		 */

		addElement: function( page_id, element_definition_id, show_elements, default_values, hidden_data, options ) {
			var modal_tag='modal_' + Math.floor(Math.random()*100000+1);
			$.ajax({
				type: "GET",
				dataType: "json",
				url: "/admin/api/element_definitions/"+element_definition_id,
				success: function(data) {
					var form=voogLab.elementEditor.renderElementEditForm(data,{page:{id:page_id}},show_elements,default_values,hidden_data,'add',modal_tag,options);
					voogLab.openModal(form,modal_tag);
				},
				error: function(xhr,ajaxOptions,thrownError) {
					alert('Error fetching element definition: '+thrownError);
				}
			});
		},


		/**
		 *   Open edit element dialog
		 *   element_id  -  Voog element ID
		 *   show_elements  -  If not null, an array of elements to show
		 *   hidden_data  -  If not null, object of hidden data to pass along with submit
		 *   options  -  Options
		 *     modal_title  -  Modal title
		 *     title_label  -  Element title label
		 */

		editElement: function( element_id, show_elements, hidden_data, options ) {
			var modal_tag='modal_' + Math.floor(Math.random()*100000+1);
			$.ajax({
				type: "GET",
				dataType: "json",
				url: "/admin/api/elements/"+element_id,
				success: function(element_data) {
					console.log(element_data);
					$.ajax({
						type: "GET",
						dataType: "json",
						url: "/admin/api/element_definitions/"+element_data.element_definition.id,
						success: function(element_definition_data) {
							var form=voogLab.elementEditor.renderElementEditForm(element_definition_data,element_data,show_elements,{},hidden_data,'edit',modal_tag,options);
							voogLab.openModal(form,modal_tag);
						},
						error: function(xhr,ajaxOptions,thrownError) {
							alert('Error fetching element definition: '+thrownError);
						}
					});
				},
				error: function(xhr,ajaxOptions,thrownError) {
					alert('Error fetching element data: '+thrownError);
				}
			});
		},


		/**
		 *   Submit element
		 *   modal_tag  -  Modal dialog tag
		 */

		submitElement: function( modal_tag ) {
			var form_type=$("#form_"+modal_tag).attr('data-form-type');
			var element_id=$("#form_"+modal_tag).attr('data-element-id');
			var element_definition_id=$("#form_"+modal_tag).attr('data-element-definition');
			var page_id=$("#form_"+modal_tag).attr('data-page');

			var element_title=$("#form_"+modal_tag+" input[name=element_title]").val().trim();
			if (element_title === '') {
				alert('Element title cannot be empty.');
				return;
			}

			var postData={};
			if (form_type == 'add') {
				postData.element_definition_id=element_definition_id;
				postData.page_id=page_id;
			}
			postData.title=element_title;
			postData.values={};
			$.each($("#form_"+modal_tag).serializeArray(),function(_, kv){
				if (kv.name != 'element_title') {
					postData.values[kv.name]=kv.value;
				}
			});

			$("#form_"+modal_tag+" .edy-form-save-btn").html('<span class="dot"></span><span class="dot"></span><span class="dot"></span>').addClass('dot-loader');

			if (form_type == 'edit') {
				$.ajax({
					type: "PUT",
					dataType: "json",
					url: "/admin/api/elements/"+element_id,
					data: JSON.stringify(postData),
					processData: false,
					contentType: 'application/json',
					success: function(data) {
						alert('Element updated.');
						voogLab.reload();
					},
					error: function(xhr,ajaxOptions,thrownError) {
						$("#form_"+modal_tag+" .edy-form-save-btn").text($("#form_"+modal_tag+" .edy-form-save-btn").attr('data-title')).removeClass('dot-loader');
						alert('Error updating element: '+thrownError);
					}
				});
			}
			else {
				$.ajax({
					type: "POST",
					dataType: "json",
					url: "/admin/api/elements",
					data: JSON.stringify(postData),
					processData: false,
					contentType: 'application/json',
					success: function(data) {
						alert('Element added.');
						voogLab.reload();
					},
					error: function(xhr,ajaxOptions,thrownError) {
						$("#form_"+modal_tag+" .edy-form-save-btn").text($("#form_"+modal_tag+" .edy-form-save-btn").attr('data-title')).removeClass('dot-loader');
						alert('Error adding element: '+thrownError);
					}
				});
			}
		},


		/**
		 *   Render element edit form
		 *   modal_tag  -  Modal dialog tag
		 */

		renderElementEditForm: function( element_definition, element_data, show_elements, default_values, hidden_data, form_type, modal_tag, options ) {
			var form='';
			form=form+'<form id="form_'+modal_tag+'" data-form-type="'+form_type+'" data-element-id="'+(form_type=='edit'?element_data.id:'')+'" data-element-definition="'+element_definition.id+'" data-page="'+element_data.page.id+'" onsubmit="return false">';

			for (var hdk in hidden_data) {
				form=form+'<input type="hidden" name="'+hdk+'" value="'+hidden_data[hdk]+'">';
			}
			form=form+'<div class="edy-form-header"><div class="edy-form-title">'+((options && options.modal_title!=null)?options.modal_title:element_definition.title)+'</div></div>';
			form=form+'<div class="edy-form-horizontal edy-form-horizontal-30-70 edy-form-horizontal-lightbg" style="margin: 15px 0px 30px 0px">';
			form=form+'<div class="edy-form-contents">';
			form=form+'<fieldset>';

			form=form+'<div class="edy-form-group">';
			form=form+'<label for="title">'+((options && options.title_label!=null)?options.title_label:'Title')+'</label>';
			form=form+' <input class="edy-form-control" id="element_title" name="element_title" placeholder="..." type="text" value="'+(form_type=='edit'?element_data.title:'')+'">';
			form=form+'</div>';

			for (var pk in element_definition.data.properties) {
				if (show_elements) {
					if (show_elements.length > 0 && show_elements.indexOf(pk) == -1) {
						continue;
					}
				}
				if (hidden_data) {
					if (hidden_data[pk] != null) {
						continue;
					}
				}
				var fld=element_definition.data.properties[pk];
				var value='';
				if (element_data != null && element_data.values != null) {
					if (element_data.values[pk] != null) {
						if (element_data.values[pk] === true) {
							value='1';
						}
						else {
							value=element_data.values[pk];
						}
					}
				}
				else {
					if (default_values != null && default_values.indexOf(pk) !== -1)
					{
						value=default_values[pk];
					}
				}

				if (fld.data_type == 'image') {
					form=form+'<div class="edy-form-group edy-page-image'+(value!=''?'':' edy-page-image-empty')+'">';
					form=form+'<label for="'+fld.key+'">'+fld.title+'</label>';
					form=form+'<div class="edy-form-valuetext">';
					form=form+'<div class="edy-page-image-container">';
					form=form+'<input id="'+fld.key+'" name="'+fld.key+'" type="hidden">';
					form=form+'<div class="edy-page-image-preview"></div>';
					form=form+'<div class="edy-page-image-select"><a class="edy-form-control-link edy-page-select-image" data-behavior="show-image-picker" href="#select-image">Choose image...</a></div>';
					form=form+'<a class="edy-page-image-remove" data-behavior="remove-image" href="#remove-image"><svg width="20" height="20" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><path d="M6 6l8 8m-8 0l8-8" stroke="currentColor" stroke-width="2" fill="none"></path></svg></a>';
					form=form+'</div>';
					form=form+'</div>';
					form=form+'</div>';
				}
				else {
					form=form+'<div class="edy-form-group">';
					form=form+'<label for="'+fld.key+'">'+fld.title+'</label>';
					if (fld.data_type == 'boolean') {
						form=form+'<div class="edy-form-control edy-checkbox">';
						form=form+'<input type="checkbox" id="'+fld.key+'" name="'+fld.key+'" value="1" '+((value==='1' || value===1 || value===true)?'checked="checked"':'')+'>';
						form=form+'</div>';
					}
					else if (fld.data_type == 'select') {
						form=form+'<div class="edy-form-control edy-select">';
						form=form+'<div class="edy-select-text">'+(value==''?'...':value)+'</div>';
						form=form+'<select id="'+fld.key+'" name="'+fld.key+'">';
						for (var o in fld.options) {
							form=form+'<option value="'+fld.options[o]+'" '+(fld.options[o]==value?'selected="selected"':'')+'>'+fld.options[o]+'</option>';
						}
						form=form+'</select>';
						form=form+'</div>';
					}
					else if (fld.data_type == 'text') {
						form=form+' <textarea class="edy-form-control" id="'+fld.key+'" name="'+fld.key+'" rows="8" placeholder="...">'+value+'</textarea>';
					}
					else {
						form=form+' <input class="edy-form-control" id="'+fld.key+'" name="'+fld.key+'" placeholder="..." type="text" value="'+value+'">';
					}
					form=form+'</div>';
				}
			}

			form=form+'</fieldset>';
			form=form+'</div>';
			form=form+'</div>';

			form=form+'<div class="edy-form-buttons edy-align-center">';
			form=form+'<button class="edy-btn edy-btn-large edy-btn-gray-dark edy-form-save-btn" name="button" type="button" onclick="voogLab.elementEditor.submitElement(\''+modal_tag+'\')" data-title="Save">Save</button>';
			form=form+'</div>';

			form=form+'</form>';
			return form;
		},


		/**
		 *   Delete element
		 *   element_id  -  Voog element ID
		 *   confirm_msg  -  true/false or confirm message as a string
		 */

		deleteElement: function ( element_id, confirm_msg ) {
			if (confirm_msg == null || confirm_msg === false) {
				$.ajax({
					type: "DELETE",
					url: "/admin/api/elements/"+element_id,
					success: function(data) {
						alert('Element deleted.');
						voogLab.reload();
					},
					error: function(xhr,ajaxOptions,thrownError) {
						alert('Error deleting element: '+thrownError);
					}
				});
			}
			else {
				if (confirm_msg === true) {
					confirm_msg='Delete?';
				}
				if (confirm(confirm_msg)) {
					voogLab.elementEditor.deleteElement(element_id,false);
				}
			}
		}


	}


};